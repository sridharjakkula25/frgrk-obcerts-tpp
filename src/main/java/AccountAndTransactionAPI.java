import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;

import constants.SandBoxConstants;
import utility.HTTPSClient;

public class AccountAndTransactionAPI {
	
	public static void main (String[] args) {
		String accessToken = SandBoxConstants.constants.getProperty("resource_access_token");
		List<BasicHeader> headers = new ArrayList<BasicHeader>();
		headers.add(new BasicHeader("Content-Type", "application/json"));
		headers.add(new BasicHeader("Authorization", "Bearer "+accessToken));
		headers.add(new BasicHeader("x-idempotency-key", "FRESCO.21302.GFX.20"));
		headers.add(new BasicHeader("x-fapi-financial-id", SandBoxConstants.constants.getProperty("financialID")));
		headers.add(new BasicHeader("x-fapi-customer-ip-address", "104.25.212.99"));
		headers.add(new BasicHeader("Accept", "application/json"));
	
		String interactionId = UUID.randomUUID().toString();
		headers.add(new BasicHeader("x-fapi-interaction-id", interactionId));
		
		
		String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
		 String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
		 String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
		 String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
		 String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
			
			
		    CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
					transportCertAlias, idStorePassword, trasportStorePassWord);
		
		
		// String accountResourceURI = SandBoxConstants.constants.getProperty("forgeRockBaseUrl")+SandBoxConstants.constants.getProperty("bulkAccountEndPoint");
		
		    String accountResourceURI = "https://rs.aspsp.ob.forgerock.financial/open-banking/v2.0/accounts";
		 String response = HTTPSClient.callGetMethod(client, accountResourceURI, headers);
		 
		 System.out.println("************ Bulk Accounts ****************");
		 System.out.println(response);
		 
		 
		
		 

	}

}
