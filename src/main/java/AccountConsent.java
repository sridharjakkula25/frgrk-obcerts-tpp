import java.io.IOException;

import org.json.JSONObject;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jwt.JWTClaimsSet;

import constants.SandBoxConstants;
import pojo.AccessToken;
import pojo.AccountRequestAccessToken;
import utility.ForgeRockUtility;
import utility.TokenUtility;

public class AccountConsent {
	
	
	public static void main(String[] args) {
		String clientId = SandBoxConstants.constants.getProperty("client_id");
		
		/********* Get Access token  *************/
		JWTClaimsSet jwtClaims = ForgeRockUtility.getForgeRockDynamicRegistrationClaims(clientId);

		JWSHeader jwsHeader = new JWSHeader
                .Builder(JWSAlgorithm.parse("RS256"))
                .type(JOSEObjectType.JWT)
                .keyID(SandBoxConstants.constants.getProperty("keyId"))               
                .build();
		
		String signedJwt = TokenUtility.getSignJwt(jwsHeader, jwtClaims);
		
		System.out.println("******* Signed JWT ***********");
		System.out.println(signedJwt);
		
		String response = ForgeRockUtility.getAccessToken(clientId, signedJwt);
		AccountRequestAccessToken accessToken = null;
		
		if (response != null && response.contains("access_token")) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				accessToken = mapper.readValue(response, AccountRequestAccessToken.class);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		System.out.println(accessToken.getAccess_token());
		
		/***** Account Request ****/
		
	    String financialId = SandBoxConstants.constants.getProperty("financialID");
	    
	    response =  ForgeRockUtility.postAccountRequest(accessToken.getAccess_token(), financialId);
		System.out.println(response);
		
		JSONObject jsonObject = new JSONObject(response);
				
		String consentId = (String) new JSONObject(jsonObject.get("Data").toString()).get("AccountRequestId");
		
		System.out.println("************** Consent ID ******************");
		System.out.println(consentId);
		
		String redirectUri = SandBoxConstants.constants.getProperty("forgeRockRedirectURI");
		String aud = SandBoxConstants.constants.getProperty("forgeRockAud-aud");
		
		String preUrlJwt = ForgeRockUtility.getPreUrlJwt(clientId, consentId, redirectUri, aud);
		
		System.out.println("************** Preurl JWT ******************");
		System.out.println(preUrlJwt);
		
	    String preAuthUrl = SandBoxConstants.constants.getProperty("forgeRockPreAuthURL");
	    preAuthUrl = preAuthUrl + "?" + "response_type=code id_token&client_id=" + clientId + 
	    		"&state=10d260bf-a7d9-444a-92d9-7b7a5f088208&nonce=10d260bf-a7d9-444a-92d9-7b7a5f088208&scope=openid "
	    		+ "accounts&redirect_uri=https://app.getpostman.com/oauth2/callback&request="
	    		+ preUrlJwt;
	    
	    System.out.println("************** Pre Auth URL request******************");
	    System.out.println(preAuthUrl);
		
		
	}
	
	

}
