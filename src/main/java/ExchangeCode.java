import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;

import constants.SandBoxConstants;
import pojo.AccountRequestAccessToken;
import pojo.CodeAccessToken;
import utility.ForgeRockUtility;

public class ExchangeCode {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String clientId = SandBoxConstants.constants.getProperty("client_id");
		String code = SandBoxConstants.constants.getProperty("code");
		
		String response = ForgeRockUtility.getResourceAccessToken(clientId, code);
		
		System.out.println(response);
		
		CodeAccessToken codeAccessToken = null;
		
		if (response != null && response.contains("access_token")) {
			ObjectMapper mapper = new ObjectMapper();
			try {
				codeAccessToken = mapper.readValue(response, CodeAccessToken.class);
				System.out.println("********** Code Access Token *****************");
				System.out.println(codeAccessToken.getAccess_token());
				SandBoxConstants.writeIntoProperties("resource_access_token", codeAccessToken.getAccess_token());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		

	}
	
	

}
