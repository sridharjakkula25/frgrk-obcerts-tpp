import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.json.JSONObject;

import constants.SandBoxConstants;
import utility.HTTPSClient;

public class PaymentInitiationAPI {
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String accessToken = SandBoxConstants.constants.getProperty("resource_access_token");
		String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
		String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
		String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
		String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
		String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
			
			
		 CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
					transportCertAlias, idStorePassword, trasportStorePassWord);
		 
		 String paymentSubmissionURI = "https://rs.aspsp.ob.forgerock.financial:443/open-banking/v2.0/payment-submissions";
		    
		 List<BasicHeader> headers = new ArrayList<BasicHeader>();
			headers.add(new BasicHeader("Content-Type", "application/json"));
			headers.add(new BasicHeader("Authorization", "Bearer "+accessToken));
			headers.add(new BasicHeader("x-idempotency-key", "FRESCO.21302.GFX.20"));
			headers.add(new BasicHeader("x-fapi-financial-id", SandBoxConstants.constants.getProperty("financialID")));
			headers.add(new BasicHeader("x-fapi-customer-ip-address", "104.25.212.99"));
			headers.add(new BasicHeader("Accept", "application/json"));
		
			String interactionId = UUID.randomUUID().toString();
			headers.add(new BasicHeader("x-fapi-interaction-id", interactionId));
			// SamplePaymentSubmissionRequest.txt
			String paymentSubmissionPayload = "";
			String paymentID = SandBoxConstants.constants.getProperty("paymentID");
			try {
				paymentSubmissionPayload = new String(Files.readAllBytes(Paths.get("SamplePaymentSubmissionRequest.txt")));
				System.out.println("****** Payment Submission Payload  *****");
				System.out.println(paymentSubmissionPayload);
				JSONObject json =new JSONObject(paymentSubmissionPayload);
				JSONObject dataJson = json.getJSONObject("Data");				
				dataJson.put("PaymentId", paymentID);				
				json.put("Data", dataJson);
				paymentSubmissionPayload = json.toString();
				System.out.println("****** Payment Submission Payload  *****");
				System.out.println(paymentSubmissionPayload);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			String response = HTTPSClient.callPostMethod(client, paymentSubmissionURI, headers, paymentSubmissionPayload);
			 
			 System.out.println("************ Payment Submission Response ****************");
			 System.out.println(response);
				
			 JSONObject jsonObject = new JSONObject(response);
			 String paymentSubmissionID = (String) new JSONObject(jsonObject.get("Data").toString()).get("PaymentSubmissionId");
				
			 String status = "";
			 
			 String paymentSubmissionStatusURL = paymentSubmissionURI + "/"+ paymentSubmissionID;			 
			 
				 response = HTTPSClient.callGetMethod(client, paymentSubmissionStatusURL, headers);			 
				 System.out.println(response);
				 jsonObject = new JSONObject(response);
				 status = (String) new JSONObject(jsonObject.get("Data").toString()).get("Status");
				 System.out.println("******************* Payment Status ********************");
				 System.out.println("Payment Status : " + status);
				 System.out.println("******************* Payment Status ********************");
				  
	}

}
