

import org.apache.http.impl.client.CloseableHttpClient;

import constants.SandBoxConstants;
import utility.HTTPSClient;

public class UnregisterClient {

	public static void main(String[] args) {
		/* Reguster TPP with ForgeRock model bank */
		String forgeRockBaseUrl = SandBoxConstants.constants.getProperty("forgeRockBaseUrl");
		String regPath = SandBoxConstants.constants.getProperty("forgeRockRegistrationEndPoint");				
		String forgeRockRegistrationPath = forgeRockBaseUrl +  regPath;
		
		
		unRegisterWithForgeRock(forgeRockRegistrationPath);

	}
	
private static void unRegisterWithForgeRock(String forgeRockRegistrationPath) {
		
	 String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
	    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
		
	    CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
				transportCertAlias, idStorePassword, trasportStorePassWord);
	    
	    HTTPSClient.callDeleteMethod(forgeRockRegistrationPath, client);
	    
	    
		
	}

}
