package constants;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;



public class SandBoxConstants {
	
	public static Properties constants = new Properties();
	public static String paymentid = "";
	
	static {
		try {
			constants.load(new FileInputStream("./config.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void writeIntoProperties(String key, String value) {
		PropertiesConfiguration config;
		try {
			config = new PropertiesConfiguration("config.properties");
			config.setProperty(key, value);
			
			config.save();
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	  }
}
