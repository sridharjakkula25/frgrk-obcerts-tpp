package utility;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Key;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.http.NameValuePair;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JOSEObjectType;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.PlainHeader;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.PlainJWT;
import com.nimbusds.jwt.SignedJWT;

import constants.SandBoxConstants;

public class ForgeRockUtility {
	
	public static  JWTClaimsSet getForgeRockDynamicRegistrationClaims(String clientID) {
			
		   	long current = System.currentTimeMillis() ;
		   	long exp = current + 300;
		      JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()		      
		      .issuer(clientID)
		      .subject(clientID)
		      .audience(SandBoxConstants.constants.getProperty("forgeRockAud"))
		      .jwtID(UUID.randomUUID().toString())   
		      .issueTime(new Date(current)) 
		      .expirationTime(new Date(exp))                                    
		      .build();
		      
		      return jwtClaimsSet;
			

			}
	
	public static String getAccessToken(String clientID, String jwt) {
		String response = "";
		List<BasicHeader> headers = new ArrayList<BasicHeader>();
		headers.add(new BasicHeader("Content-Type", "application/x-www-form-urlencoded"));
		
		 String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
		 String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
		 String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
		 String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
		 String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
			
			
		    CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
					transportCertAlias, idStorePassword, trasportStorePassWord);
		    
		    String accessTokenUrl = SandBoxConstants.constants.getProperty("forgeRockAccessTokenUrl");
		    
		    List<NameValuePair> form = new ArrayList<>();
     		form.add(new BasicNameValuePair("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer"));
     		form.add(new BasicNameValuePair("grant_type", "client_credentials"));
     		form.add(new BasicNameValuePair("client_id", clientID));
     		form.add(new BasicNameValuePair("client_assertion", jwt));
     		form.add(new BasicNameValuePair("scope", "openid accounts payments"));
		    
     		response = HTTPSClient.postClient(client, accessTokenUrl, headers, form);
     		
     		System.out.println(response);   		
     		return response;
	}
	
	public static String postAccountRequest(String accessToken, String financialId) {
		String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
	    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
		
		
		CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
				transportCertAlias, idStorePassword, trasportStorePassWord);
		
		List<BasicHeader> headers = new ArrayList<BasicHeader>();
		headers.add(new BasicHeader("Content-Type", "application/json"));
		headers.add(new BasicHeader("x-fapi-financial-id", financialId));
		headers.add(new BasicHeader("x-fapi-interaction-id", "34567"));
		headers.add(new BasicHeader("Authorization", "Bearer "+accessToken));
		headers.add(new BasicHeader("x-fapi-customer-ip-address", "104.25.212.99"));
		String accountRequestBody  = "";
		
		try {
			accountRequestBody = new String(Files.readAllBytes(Paths.get("account_request_permissions.txt")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String accountUrl = SandBoxConstants.constants.getProperty("forgeRockAccountRequest");				
		String response = HTTPSClient.callPostMethod(client, accountUrl, headers, accountRequestBody);		
		return response;
		
	}
	
	public static String postPaymentRequest(String accessToken, String financialId) {
		
		
		String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	    String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	    String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
	    String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	    String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
		
		
		CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
				transportCertAlias, idStorePassword, trasportStorePassWord);
		
		List<BasicHeader> headers = new ArrayList<BasicHeader>();
		headers.add(new BasicHeader("Content-Type", "application/json"));
		headers.add(new BasicHeader("x-fapi-financial-id", financialId));
		headers.add(new BasicHeader("x-fapi-interaction-id", UUID.randomUUID().toString()));
		headers.add(new BasicHeader("Authorization", "Bearer "+accessToken));
		headers.add(new BasicHeader("x-fapi-customer-ip-address", "104.25.212.99"));
		headers.add(new BasicHeader("x-idempotency-key", "FRESCO.21302.GFX.20"));
		headers.add(new BasicHeader("Accept", "application/json"));
		String paymentRequestBody  = "";
		
		try {
			paymentRequestBody = new String(Files.readAllBytes(Paths.get("SamplePaymentRequest.txt")));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String paymentUrl = SandBoxConstants.constants.getProperty("forgeRockPaymentRequest");				
		String response = HTTPSClient.callPostMethod(client, paymentUrl, headers, paymentRequestBody);		
		return response;
		
	}
	
	 public static String getPreUrlJwt(String clientId, String accountId, 
				String redirectUri, String aud)  {			
			
			JWK jwk = null;
			SignedJWT signedJWT = null;
			String signingPrivateKey = SandBoxConstants.constants.getProperty("singingPrivateKey");
			String cert = SandBoxConstants.constants.getProperty("signingPublicCertificate");
			
			System.out.println(signingPrivateKey);
			System.out.println(cert);
			Key privateKey = TokenUtility.getPrivateKey(signingPrivateKey);
			
			RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;			
			RSAPublicKey rsaPublicKey = (RSAPublicKey) TokenUtility.getPublicKey(cert);					
			
			jwk = new RSAKey.Builder(rsaPublicKey)
				    .privateKey( rsaPrivateKey)
				    .keyID(UUID.randomUUID().toString()) 
				    .build();									
			
			JWSHeader jwsHeader = new JWSHeader
	                .Builder(JWSAlgorithm.parse("RS256"))
	                .type(JOSEObjectType.JWT)
	                .keyID(SandBoxConstants.constants.getProperty("keyId"))               
	                .build();
			
			
			JWTClaimsSet jwtClaimsSet = getPreUrlJwtClaims(clientId, accountId, redirectUri, aud);
			 signedJWT = new SignedJWT(jwsHeader, jwtClaimsSet);
		        try {
					signedJWT.sign(new RSASSASigner((RSAKey) jwk));
				} catch (JOSEException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return signedJWT.serialize();
			
			}
	
	
	
	private static JWTClaimsSet getPreUrlJwtClaims(String clientId, String accountId, 
			String redirectUri, String aud) {

		long current = System.currentTimeMillis() ;
		
		long exp = current + 7200000;
		String claims = "";
		JSONObject jsonObj = null;
		
	
		
		JSONObject claims_string = getPreUrlCaims(accountId);
		
		System.out.println(claims_string.toString());
		
		JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
					  .issuer(clientId)
					  .claim("scope", "accounts openid payments")
					  .claim("claims", claims_string.toMap())
					  .issuer(clientId)
					  .claim("client_id", clientId)
					  .expirationTime(new Date(exp))
					  .issueTime(new Date(current))
					  .claim("response_type", "code id_token")
					  .claim("redirect_uri", redirectUri)
					  .jwtID(UUID.randomUUID().toString())  
					  .audience("https://matls.as.aspsp.ob.forgerock.financial/oauth2/openbanking")
					  .build();
		return jwtClaimsSet;
		
		}
  private static JSONObject getPreUrlCaims(String accountRequest) {
	  
	  JSONObject jsonObject = new JSONObject();
	  JSONObject userInfo = new JSONObject();
	  JSONObject acr = new JSONObject();
	  
	  	acr.put("essential", true);
	  	acr.put("value", "urn:openbanking:psd2:sca");
		jsonObject.put("essential", true);
		jsonObject.put("value", accountRequest);
		
		JSONObject jsonObject1 = new JSONObject();
		jsonObject1.put("openbanking_intent_id", jsonObject );
		jsonObject1.put("acr", acr);
		
		JSONObject idToken1 = new JSONObject();		
		userInfo.put("openbanking_intent_id", jsonObject);

		idToken1.put("id_token", jsonObject1);
		idToken1.put("userinfo", userInfo);
		
	//	userInfoJson.put("id_token", idToken);
		
		System.out.println("**** claims ");
		System.out.println(idToken1);
		
		return idToken1;
	  
  }
  
  public static String getResourceAccessToken(String clientId, String code) {
	  
	  List<BasicHeader> headers = new ArrayList<BasicHeader>();
	  headers.add(new BasicHeader("Content-Type", "application/x-www-form-urlencoded"));
	  
	  String jwt = getAccessTokenJwt(clientId);
	  
	  List<NameValuePair> form = new ArrayList<>();
	  form.add(new BasicNameValuePair("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer"));
	  form.add(new BasicNameValuePair("redirect_uri", "https://app.getpostman.com/oauth2/callback"));
	  form.add(new BasicNameValuePair("grant_type", "authorization_code"));
	  form.add(new BasicNameValuePair("code", code));
	  form.add(new BasicNameValuePair("client_assertion", jwt));
	  
	  String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	  String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	  String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
 	  String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	  String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
			
			
	  CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
					transportCertAlias, idStorePassword, trasportStorePassWord);
		    
	  String accessTokenUrl = SandBoxConstants.constants.getProperty("forgeRockHybridAccessTokenUrl");
	   
	   
	  String response = HTTPSClient.postClient(client, accessTokenUrl, headers, form);	  
	  return response;	  
  }
  
  private static String getAccessTokenJwt (String clientID) {
	  String jwt = "";	  
	  JWK jwk = null;
		SignedJWT signedJWT = null;
		String signingPrivateKey = SandBoxConstants.constants.getProperty("singingPrivateKey");
		String cert = SandBoxConstants.constants.getProperty("signingPublicCertificate");
		
		System.out.println(signingPrivateKey);
		System.out.println(cert);
		Key privateKey = TokenUtility.getPrivateKey(signingPrivateKey);
		
		RSAPrivateKey rsaPrivateKey = (RSAPrivateKey) privateKey;			
		RSAPublicKey rsaPublicKey = (RSAPublicKey) TokenUtility.getPublicKey(cert);					
		
		jwk = new RSAKey.Builder(rsaPublicKey)
			    .privateKey( rsaPrivateKey)
			    .keyID(UUID.randomUUID().toString()) 
			    .build();									
		
		JWSHeader jwsHeader = new JWSHeader
              .Builder(JWSAlgorithm.parse("RS256"))
              .type(JOSEObjectType.JWT)
              .keyID(SandBoxConstants.constants.getProperty("keyId"))               
              .build();
	  
		long current = System.currentTimeMillis() ;
		
		long exp = current + 300;
		String claims = "";
		JSONObject jsonObj = null;
		
		JWTClaimsSet jwtClaimsSet = new JWTClaimsSet.Builder()
									.issuer(clientID)
									.subject(clientID)
									.issueTime(new Date(current))
									.expirationTime(new Date(exp))
									.audience("https://matls.as.aspsp.ob.forgerock.financial/oauth2/openbanking")
									//.claim("aud", "https://matls.as.aspsp.ob.forgerock.financial/oauth2/openbanking")
									.jwtID(UUID.randomUUID().toString())
									.build();
		
		
		  SignedJWT signedJwt =  new SignedJWT(jwsHeader, jwtClaimsSet);
		  
		  
	      try {
	    	   System.out.println(jwk);
	    	   signedJwt.sign(new RSASSASigner((RSAKey) jwk));
			} catch (JOSEException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	  
	  return signedJwt.serialize();
  }
  
  public static String testMtls() {
	  String response = "";
	  
	  String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
	  String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
	  String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
 	  String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
	  String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
			
			
	  CloseableHttpClient client = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
					transportCertAlias, idStorePassword, trasportStorePassWord);
		    
	  String testMtlsUrl = SandBoxConstants.constants.getProperty("mtlsEndPoint");
	  
	  List<BasicHeader> headers = new ArrayList<BasicHeader>();
	  
	  
	  
	  response = HTTPSClient.callGetMethod(client, testMtlsUrl, headers);
		 
	  
	  
	  return response;
  }
	
	

}
