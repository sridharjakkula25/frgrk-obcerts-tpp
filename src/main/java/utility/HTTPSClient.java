package utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.List;

import javax.net.ssl.SSLContext;
import org.apache.http.Consts;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;


import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;



public class HTTPSClient {
		
	public static CloseableHttpClient getHttpClient(String pathToTrustrStore, String pathToIdStore,
							String certAlias, String idStorePassword, String trustStorePassWord) {
		 
	      
	      KeyStore identityKeyStore;
	      CloseableHttpClient client = null;
		try {
			identityKeyStore = KeyStore.getInstance("jks");
		    FileInputStream identityKeyStoreFile = new FileInputStream(new File(pathToIdStore));
		      identityKeyStore.load(identityKeyStoreFile, idStorePassword.toCharArray());
		 
		      KeyStore trustKeyStore = KeyStore.getInstance("jks");
		      FileInputStream trustKeyStoreFile = new FileInputStream(new File(pathToTrustrStore));
		      trustKeyStore.load(trustKeyStoreFile, trustStorePassWord.toCharArray());
		      
		     
		     
		     SSLContext sslContext = SSLContexts.custom()
		             // load identity keystore
		             .loadKeyMaterial(identityKeyStore, idStorePassword.toCharArray())
		             // load trust keystore
		             .loadTrustMaterial(trustKeyStore, null)
		             .build();
		     
		      
		      SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext,
		          new String[]{"TLSv1.2", "TLSv1.1"},
		    		
		          null,
		          SSLConnectionSocketFactory.getDefaultHostnameVerifier());
		          
		      
		      client = HttpClients.custom()
		          .setSSLSocketFactory(sslConnectionSocketFactory)
		          .build();
		} catch (KeyStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CertificateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (KeyManagementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
		
		return client;
	}
		
	
	public static String callGetMethod(CloseableHttpClient aHTTPClient, String aEndPointURL,
							List<BasicHeader> headers) {
		StringBuffer response = new StringBuffer();
		
		System.out.println ("GET : url end point : " + aEndPointURL);
		System.out.println("GET : Headers ");
		
		HttpGet httpGet = new HttpGet(aEndPointURL);
		 for (BasicHeader header : headers){
			httpGet.addHeader(header);
			System.out.println("GET : Headers " + header.getName() + " : " + header.getValue() );
		
		 }
			
			BufferedReader reader = null;
		
		CloseableHttpResponse httpResponse;
		try {
			
			httpResponse = aHTTPClient.execute(httpGet);
//			System.out.println(httpResponse.getStatusLine());
			

			 reader = new BufferedReader(new InputStreamReader(
					httpResponse.getEntity().getContent()));

			String inputLine;
			
			while ((inputLine = reader.readLine()) != null) {
				response.append(inputLine);
			}
			
	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
				try {
					reader.close();
					aHTTPClient.close();
				} catch (IOException e) {
					
				}
			
		}

		return response.toString();
	}
	
	public static String callPostMethod (CloseableHttpClient aHTTPClient, 
			 						String aEndPointURL, List<NameValuePair> form) {
		 	  StringBuffer sb = new StringBuffer();
		    
		 	  HttpPost post = new HttpPost(aEndPointURL);
		      post.setHeader("Accept", "*/*");
		      post.setHeader("Content-Type", "application/x-www-form-urlencoded");
		      post.setHeader("Accept-Encoding", "gzip, deflate");
		      UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, Consts.UTF_8);
		      
		      post.setEntity(entity);		      
		    
		      
	
		      
		      HttpResponse response;
			try {
				
				response = aHTTPClient.execute(post);
			
		  
		      int responseCode = response.getStatusLine().getStatusCode();
		
		      BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		      String line = "";
		      
		      while ((line = rd.readLine()) != null) {
		       //   System.out.println(line);
		          sb.append(line);
		      }
		      
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		    return sb.toString();
		    
		  }  
	public static String postClient (CloseableHttpClient aHTTPClient, 
				String aEndPointURL, List<BasicHeader> headers, List<NameValuePair> form) {
			StringBuffer sb = new StringBuffer();
			
			HttpPost post = new HttpPost(aEndPointURL);
			 for (BasicHeader header : headers){
					post.addHeader(header);
					System.out.println("POST : Headers " + header.getName() + " : " + header.getValue() );
				
				 }
			 for (NameValuePair np : form) {
				 System.out.printf("Name:" + np.getName() + " value: " + np.getValue()+ "\n");
			 }

			UrlEncodedFormEntity entity = new UrlEncodedFormEntity(form, Consts.UTF_8);			
			post.setEntity(entity);		      
			HttpResponse response;
			try {
			
			response = aHTTPClient.execute(post);						
			int responseCode = response.getStatusLine().getStatusCode();
			
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			String line = "";
			
			while ((line = rd.readLine()) != null) {
			//   System.out.println(line);
			sb.append(line);
			}
			
			} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			}
			
			return sb.toString();
	} 
	
	public static String callPostMethod(CloseableHttpClient aHTTPClient, 
				String aEndPointURL, List<BasicHeader> headers, String requestBody) {
		
		
		StringBuffer responseBody = new StringBuffer();
		
		System.out.println ("POST : url end point: " + aEndPointURL);
		System.out.println("POST : Headers " + headers.toArray()[0].toString());
		System.out.println("body :" + requestBody);
		
		
		HttpPost httpPost = new HttpPost(aEndPointURL);
		 for (BasicHeader header : headers){
			httpPost.addHeader(header);
			System.out.println("POST : Headers " + header.getName() + " : " + header.getValue() );
		
		 }
		
		 
			
		 try {
			 httpPost.setEntity(new StringEntity(requestBody));
			 HttpResponse response = aHTTPClient.execute(httpPost);
			 
			 System.out.println("************response headers**************");
            Header[] headers1 = response.getAllHeaders();
            for (Header header : headers1) {
        		System.out.println("Key : " + header.getName() 
        		      + " ,Value : " + header.getValue());
        	}
		  
		      int responseCode = response.getStatusLine().getStatusCode();
		
		      BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		      String line = "";
		      
		      while ((line = rd.readLine()) != null) {
		       //   System.out.println(line);
		    	  responseBody.append(line);
		      }
		      
			} catch (ClientProtocolException e) { 
				// TODO Auto-generated catch block
				System.out.println("Client Protocol Exception " + e.getMessage());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("IO exception " + e.getMessage());
				e.printStackTrace();
			}
		    
		return responseBody.toString();
	}
	
	public static void callDeleteMethod(String regEndPoint, CloseableHttpClient client) {
		
		URI uri;
		try {
			
			    
			uri = new URI(regEndPoint);
			HttpDelete httpDelete = new HttpDelete(uri);
			System.out.println("Executing request " + httpDelete.getRequestLine());

            // Create a custom response handler
            ResponseHandler<String> responseHandler = response -> {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            };
            String responseBody = client.execute(httpDelete, responseHandler);
            System.out.println("----------------------------------------");
            System.out.println(responseBody);
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	
	public static String callPostMethod(CloseableHttpClient aHTTPClient, 
			String aEndPointURL, List<BasicHeader> headers, List<NameValuePair> form) {
	
	
	StringBuffer responseBody = new StringBuffer();
	
	System.out.println ("POST : url end point : " + aEndPointURL);
	System.out.println("POST : Headers ");
	
	HttpPost httpPost = new HttpPost(aEndPointURL);
	 for (BasicHeader header : headers){
		httpPost.addHeader(header);
		System.out.println("POST : Headers " + header.getName() + " : " + header.getValue() );
	
	 }
	 for (NameValuePair np : form) {
		 System.out.println("request-body:" + np.getName() + ":" + np.getValue());
	 }
	 
		
	 try {
		 httpPost.setEntity(new UrlEncodedFormEntity(form, Consts.UTF_8));
		 HttpResponse response = aHTTPClient.execute(httpPost);
		
	  
	      int responseCode = response.getStatusLine().getStatusCode();
	
	      BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
	      String line = "";
	      
	      while ((line = rd.readLine()) != null) {
	       //   System.out.println(line);
	    	  responseBody.append(line);
	      }
	      
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    
	return responseBody.toString();
}
	 
	
	
	public static CloseableHttpClient getHttpClientWithNoHostValidation(String pathToTrustrStore, String pathToIdStore,
			String certAlias, String idStorePassword, String trustStorePassWord) {


			 KeyStore identityKeyStore;
		      CloseableHttpClient client = null;
			try {
				identityKeyStore = KeyStore.getInstance("jks");
			    FileInputStream identityKeyStoreFile = new FileInputStream(new File(pathToIdStore));
			      identityKeyStore.load(identityKeyStoreFile, idStorePassword.toCharArray());
			 
			      KeyStore trustKeyStore = KeyStore.getInstance("jks");
			      FileInputStream trustKeyStoreFile = new FileInputStream(new File(pathToTrustrStore));
			      trustKeyStore.load(trustKeyStoreFile, trustStorePassWord.toCharArray());
			      
			     
			     
			     SSLContext sslContext = SSLContexts.custom()
			             // load identity keystore
			             .loadKeyMaterial(identityKeyStore, idStorePassword.toCharArray())
			             // load trust keystore
			             .loadTrustMaterial(trustKeyStore, null)
			             .build();
			     
			      
			      SSLConnectionSocketFactory sslConnectionSocketFactory = new SSLConnectionSocketFactory(sslContext,
			          new String[]{"TLSv1.2", "TLSv1.1"},
			    		
			          null,
			          SSLConnectionSocketFactory.getDefaultHostnameVerifier());
			          
			      
			      client = HttpClients.custom()
			          .setSSLSocketFactory(sslConnectionSocketFactory)
			          .build();
			} catch (KeyStoreException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CertificateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (KeyManagementException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UnrecoverableKeyException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		  
			
			return client;
	}
	
	
	
	

}
