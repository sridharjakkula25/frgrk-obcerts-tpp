package utility;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


import constants.SandBoxConstants;
import utility.HTTPSClient;

public class SandBoxUtils {

	
	
	public static String getOBAccountPaymantServiceProviders(String accessToken) {
		 String trustKeyStore = SandBoxConstants.constants.getProperty("trustKeyStore");
		 String idKeyStore = SandBoxConstants.constants.getProperty("idKeyStore");
		 String transportCertAlias = SandBoxConstants.constants.getProperty("id-transport-cert-alias");
		 String idStorePassword = SandBoxConstants.constants.getProperty("id-store-password");
		 String trasportStorePassWord = SandBoxConstants.constants.getProperty("transport-store-password");
		
		CloseableHttpClient aHTTPClient = HTTPSClient.getHttpClient(trustKeyStore, idKeyStore,
				transportCertAlias, idStorePassword, trasportStorePassWord);
		BasicHeader[] headers = {new BasicHeader("Authorization", 
		         "Bearer "+accessToken)};
		String resourceUrl = SandBoxConstants.constants.getProperty("tppTestUrl");
		String response = HTTPSClient.callGetMethod(aHTTPClient, resourceUrl, 
											Arrays.asList(headers));
		
		
		System.out.println("***************** Resource details **************\n");
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		JsonParser jp = new JsonParser();
		JsonElement je = jp.parse(response);
		String prettyJsonString = gson.toJson(je);
		System.out.println(prettyJsonString);
		
		Path path = Paths.get("./output.txt");
		 
		//Use try-with-resource to get auto-closeable writer instance
		try (BufferedWriter writer = Files.newBufferedWriter(path))
		{
		    writer.write(prettyJsonString);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		return response;
		
	}
	
	
	
	

}
